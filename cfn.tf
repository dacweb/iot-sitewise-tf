data "local_file" "original.yaml" {
    filename = "/original.yaml"
}

resource "aws_cloudformation_stack" "IoT_SiteWise" {
    name = "networking-stack"

    parameters = {
        VPCCidr = var.vpc_cidr
    }

    template_body = yamlencode(data.local_file.original.yaml.content)
}